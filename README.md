# NoToastAlone
A collection of misc features for the Fully Toasted vanilla server

## Mystery Dust dropping from mobs
If the MysteryBoxes plugin is installed, mobs can now drop Mystery Dust when killed. The droprate is configurable in a `drops.yml` file, and even supports multiple drops at difference chances.

An example of the configuration:
```yml
zombie:            # Mob
  50:              # Chance
    dust: 1-5      # Dust amount
enderdragon:
  1:               # 1% chance to give bonus dust
    dust: 200-300  
  100:             # If the chance fails, 100% chance to give normal dust
    dist: 100-200
```

## JavaScript command
Allows the user to run JavaScript/code at runtime. This is useful because it allows you to run (your) existing Java code in realtime - giving you the ability to debug the result of functions and methods much more easily. After the command is run, it will tell you the return result, if any, or an error.

There are lines in the config that are automatically run when the engine is initialized. This allows you to auto import or set global variables if you wish. By default, this just imports the ChatColor and Bukkit classes so they can be used without prefixing them with `org.bukkit.`

In future, there will be the ability to create scripts, edit and execute them - all from in game. 

### Example
This example shows creating a d20 function in JavaScript, using Java's random class.
```js
/javascript run var rand = new java.util.Random();
```
(No return)
```js
/javascript run function d20() { return "You roll a " + (rand.nextInt(20) + 1) + "!"; }
```
(No return)
```
/javascript run d20();
/javascript run d20();
/javascript run d20();
```
*You roll a 19!*    
*You roll a 6!*    
*You roll a 17!*    


