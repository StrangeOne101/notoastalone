package com.fullytoasted.notoastalone.command;

import com.fullytoasted.notoastalone.NashornEngine;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.Arrays;
import java.util.List;

public class JavaScriptCommand implements CommandExecutor, TabCompleter {

	public static final String PREFIX = ChatColor.YELLOW + "[JS] ";

	@Override
	public boolean onCommand(CommandSender sender, Command command, String name, String[] args) {
		if (!sender.hasPermission("notoastalone.javascript")) {
			sender.sendMessage(ChatColor.RED + "You don't have permission to run this command!");
			return true;
		}

		if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
			sender.sendMessage(ChatColor.YELLOW + "=== JavaScript Commands ===");
			sender.sendMessage(ChatColor.YELLOW + "/javascript run <code>");
			sender.sendMessage(ChatColor.YELLOW + "/javascript reset");
			sender.sendMessage(ChatColor.YELLOW + "/javascript script [...]");
			return true;
		}

		if (!Arrays.asList(new String[] {"run", "reset", "script", "execute"}).contains(args[0].toLowerCase())) {
			sender.sendMessage(PREFIX + ChatColor.RED + "Incorrect usage! Use /javascript help");
			return true;
		}

		if (args[0].equalsIgnoreCase("run") || args[0].equalsIgnoreCase("execute")) {
			if (args.length == 1) {
				sender.sendMessage(PREFIX + ChatColor.RED + "Not enough parameters! You need to include code to run!");
				return true;
			}

			String[] arrayCopy = Arrays.copyOfRange(args, 1, args.length);
			String line = String.join(" ", arrayCopy);

			Object returned = NashornEngine.execute(line, sender);

			if (returned != null) {
				sender.sendMessage(PREFIX + returned.toString());
			} else {
				sender.sendMessage(PREFIX + ChatColor.RED + "(No return)");
			}
			return true;
		} else if (args[0].equalsIgnoreCase("reset")) {
			NashornEngine.reset();
			sender.sendMessage(PREFIX + "JavaScript engine reset!");
		} else if (args[0].equalsIgnoreCase("script")) {
			sender.sendMessage(PREFIX + "Coming soon!");
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
		return null;
	}
}
