package com.fullytoasted.notoastalone;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.List;

public class NashornEngine {

	private static ScriptEngine nashorn;

	static {
		reset();
	}

	public static void reset() {
		ClassLoader cl = NoToastAlone.class.getClassLoader(); //These allow us to use all the classes in the current
		Thread.currentThread().setContextClassLoader(cl);     //runtime in JS. Without it, only JRE classes can be used

		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		nashorn = scriptEngineManager.getEngineByName("nashorn");

		if (NoToastAlone.getPlugin().getConfig() != null && NoToastAlone.getPlugin().getConfig().contains("javascript-autorun")) {
			List<String> lines = NoToastAlone.getPlugin().getConfig().getStringList("javascript-autorun");

			boolean flag = false;

			for (String line : lines) {
				try {
					nashorn.eval(line);
				} catch (Exception e) {
					if (!flag) {
						NoToastAlone.getPlugin().getLogger().warning("Encountered errors while resetting JS engine");
						flag = true;
					}
					NoToastAlone.getPlugin().getLogger().warning(e.getMessage());
				}
			}
		}
	}

	public static Object execute(String line, CommandSender sender) {
		try {
			if (sender != null) {
				nashorn.put("player", sender);
				nashorn.put("sender", sender);
			}

			return nashorn.eval(line);
		} catch (ScriptException e) {

			return "L" + e.getColumnNumber() + ": " + e.getMessage();
		}
	}

	public static Object execute(String line) {
		return execute(line, null);
	}
}
