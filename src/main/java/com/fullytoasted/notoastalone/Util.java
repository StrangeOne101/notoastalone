package com.fullytoasted.notoastalone;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Util {

	/**
	 * Copies a resource located in the jar to a file.
	 *
	 * @param plugin The plugin
	 * @param resourceName The filename of the resource to copy
	 * @param output The file location to copy it to. Should not exist.
	 * @return True if the operation succeeded.
	 */
	public static boolean saveResource(JavaPlugin plugin, String resourceName, File output) {
		if (plugin.getResource(resourceName) == null) return false;
		output.mkdirs();

		try {
			InputStream in = plugin.getResource(resourceName);

			if (!output.exists()) {
				output.createNewFile();
			}

			OutputStream out = new FileOutputStream(output);
			byte[] buf = new byte[256];
			int len;

			while ((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}

			out.close();
			in.close();

			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean isInteger(String string) {
		try {
			Integer.parseInt(string);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
