package com.fullytoasted.notoastalone;

import com.google.common.reflect.Reflection;
import com.google.gson.internal.reflect.ReflectionAccessor;
import de.slikey.effectlib.util.ReflectionHandler;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class DropManager implements Listener {

	protected FileConfiguration dropsConfig;
	private Random random;

	//private Map<Class<? extends LivingEntity>, String> nameClassMap = new HashMap<Class<? extends LivingEntity>, String>();

	public DropManager() {
		reload();

		random = new Random();


	}

	public void reload() {
		File dropsYML = new File(NoToastAlone.getPlugin().getDataFolder(), "drops.yml");

		if (!dropsYML.exists()) {
			Util.saveResource(NoToastAlone.getPlugin(), "drops.yml", dropsYML);
		}

		dropsConfig = YamlConfiguration.loadConfiguration(dropsYML);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDeath(EntityDeathEvent event) {
		if (event.getEntity().getKiller() == null) return; //Not killed by a player
		if (event.getEntity().hasMetadata("spawnermob")) return; //Spawned from a spawner

		Player killer = event.getEntity().getKiller();

		String name = event.getEntity().getClass().getSimpleName().toLowerCase();

		if (name.startsWith("craft")) name = name.substring(5);

		if (event.getEntity() instanceof Slime) {
			Slime slime = (Slime) event.getEntity();
			String[] sizes = {"small", "medium", "large", "large", "xl", "xl"}; //Large is actually 3, not 2
			if (slime.getSize() - 1 < sizes.length) {
				String slimeOrMagmaSlime = event.getEntity() instanceof MagmaCube ? "magmacube" : "slime";
				name = sizes[slime.getSize() - 1] + "-" + slimeOrMagmaSlime;
			}
		}

		if (dropsConfig.contains(name)) {
			for (String chance : dropsConfig.getConfigurationSection(name).getKeys(false)) {
				if (Util.isInteger(chance) && random.nextInt(100) <= Integer.parseInt(chance)) { //If the chance occurs
					if (dropsConfig.contains(name + "." + chance + ".dust") && NoToastAlone.hasMysteryBox()) {
						int amount = getAmount(dropsConfig.getString(name + "." + chance + ".dust"));
						if (amount == 0) continue;
						NoToastAlone.getDustManager().addDust(killer, amount);

						String message = dropsConfig.getString("dust-grant-message");
						message = message.replace("{amount}", amount + "")
								.replace("{mob}", event.getEntity().getName());

						if (!message.isEmpty()) killer.sendMessage(ChatColor.translateAlternateColorCodes('&', message));

						break;
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onSpawn(SpawnerSpawnEvent event) {
		if (event.isCancelled()) return;
		event.getEntity().setMetadata("spawnermob", new FixedMetadataValue(NoToastAlone.getPlugin(),true));
	}

	/**
	 * Gets an amount from a string. If it's not a real number, it will try
	 * parse between 2 values. E.g. 1-5 = between 1 and 5
	 * @param string The amount
	 * @return Amount as an int
	 */
	public int getAmount(String string) {
		if (Util.isInteger(string)) return Integer.parseInt(string);

		if (string.contains(" ")) string = string.replaceAll(" ", "");
		if (string.contains("-")) {
			String s1 = string.split("-")[0];
			String s2 = string.split("-")[1];

			if (Util.isInteger(s1) && Util.isInteger(s2)) {
				int i1 = Integer.parseInt(s1);
				int i2 = Integer.parseInt(s2);

				if (i1 > i2) { //Swap values
					int temp = i1;
					i1 = i2;
					i2 = temp;
				}

				return random.nextInt(i2 - i1 + 1) + i1;
			}
		}

		return 0;
	}

	public FileConfiguration getDropsConfig() {
		return dropsConfig;
	}
}
