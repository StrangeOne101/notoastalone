package com.fullytoasted.notoastalone;

import me.xericker.mysteryboxes.playerdata.data.DataManager;
import org.bukkit.entity.Player;

public class DustManager {

	public DustManager() {

	}

	public void addDust(Player player, int amount) {
		DataManager.getData(player).setDust(DataManager.getData(player).getDust() + amount);
	}
}
