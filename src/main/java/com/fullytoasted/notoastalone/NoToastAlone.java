package com.fullytoasted.notoastalone;

import com.fullytoasted.notoastalone.command.JavaScriptCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class NoToastAlone extends JavaPlugin {

	private static NoToastAlone plugin;

	private static DustManager dustManager;
	private static DropManager dropManager;



	@Override
	public void onEnable() {
		plugin = this;

		dropManager = new DropManager();

		if (hasMysteryBox()) {
			dustManager = new DustManager();
		}

		Bukkit.getPluginManager().registerEvents(dropManager, this);

		if (!(new File(getDataFolder(), "config.yml").exists())) {
			Util.saveResource(this, "config.yml", new File(getDataFolder(), "config.yml"));
		}

		JavaScriptCommand jsCommand = new JavaScriptCommand();
		getCommand("javascript").setExecutor(jsCommand);
		getCommand("javascript").setTabCompleter(jsCommand);

		getLogger().info("NoToastAlone enabled!");
	}

	public static boolean hasMysteryBox() {
		return Bukkit.getPluginManager().isPluginEnabled("MysteryBoxes");
	}

	public static NoToastAlone getPlugin() {
		return plugin;
	}

	public static DustManager getDustManager() {
		return dustManager;
	}

	public static DropManager getDropManager() {
		return dropManager;
	}
}
